<?php
namespace Bach\Information\Controller\Index2;

use Magento\Framework\App\Action\Action;

class Test extends Action
{
    protected $_resultJsonFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->_resultJsonFactory = $resultJsonFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->_resultJsonFactory->create();

        $data = [
            'Name' =>'An Xuan Bach',
            'Age' => '28',
            'Information' => 'avnadadkasdjw',
        ];

        $result->setData($data);

        return $result;
    }
}
