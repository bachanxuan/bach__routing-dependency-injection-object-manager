<?php

namespace Bach\Information\Controller\Index3;

use Magento\Framework\App\Action\Action;

class Test extends Action
{
    protected $_pageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    ) {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $this->_pageFactory->create();
        return $this->_redirect('/');
    }
}
