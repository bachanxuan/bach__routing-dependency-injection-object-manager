<?php

namespace Bach\Information\Controller\Index4;

use Magento\Framework\App\Action\Action;

class Test extends Action
{
//    private $_resultForwardFactory;
//    private $pageFactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context
//        \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory
    ) {
//        $this->pageFactory = $pageFactory;
//        $this->_resultForwardFactory = $resultForwardFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $this->_forward("Test", 'Index3');
    }
}
